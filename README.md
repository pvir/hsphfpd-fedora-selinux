hsphfpd-fedora-selinux
======================

Selinux module for hsphfpd for Fedora.  This is just a simple Selinux module
that punches a specific hole required for hsphfpd to work.

The hsphfpd process should run in unconfined user/service SELinux context for
this to work. An example service file setting the SELinux context is included.

Install:

    sudo dnf install selinux-policy-devel
    make
    sudo semodule -i hsphfpd.pp 

Uninstall:

    sudo semodule -r hsphfpd

